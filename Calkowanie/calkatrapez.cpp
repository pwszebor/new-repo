#include "calkatrapez.h"
#include "exception.h"

CalkaTrapez::CalkaTrapez(double xmin, double xmax, Funkcja *f)
    : Calka(xmin, xmax, f)
{

}

CalkaTrapez::~CalkaTrapez()
{

}

double CalkaTrapez::obliczWartosc()
{
    try
    {
        if(getMin() > getMax())
            throw Exception(BLEDNE_DANE);
        else
        {
            double wynik = 0;
            double dx = 0.00001;
            double x1 = getMin();
            double x2 = getMax();

            while(getMin() < getMax())
            {
                if(getMin() + dx > getMax())
                    dx = getMax() - getMin();

                wynik += dx * (getF()->wartosc(getMin()) + getF()->wartosc(getMin() + dx)) / 2;

                setMin(getMin() + dx);
            }

            setMin(x1);
            setMax(x2);
            return wynik;
        }
    }
    catch(Exception &)
    {
        throw;
    }
}
