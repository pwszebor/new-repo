#ifndef CALKA_H
#define CALKA_H

#include "funkcja.h"
#include "dialog.h"

class Calka
{
public:
    Calka(double xmin, double xmax, Funkcja *f)
        : x_min(xmin),
          x_max(xmax),
          funkcja(f)
    {}
    virtual ~Calka();

    virtual double obliczWartosc() = 0;

    void rysuj(Dialog &dialog);

    double getMin() { return x_min; }
    double getMax() { return x_max; }
    Funkcja *getF() { return funkcja; }

    void setMin(double xmin) { x_min = xmin; }
    void setMax(double xmax) { x_max = xmax; }
    void setF(Funkcja *f) { funkcja = f; }

private:
    double x_min;
    double x_max;
    Funkcja *funkcja;
};

#endif // CALKA_H
