#ifndef CALKATRAPEZ_H
#define CALKATRAPEZ_H

#include "calka.h"

class CalkaTrapez : public Calka
{
public:
    CalkaTrapez(double xmin, double xmax, Funkcja *f);
    ~CalkaTrapez();

    double obliczWartosc();
};

#endif // CALKATRAPEZ_H
