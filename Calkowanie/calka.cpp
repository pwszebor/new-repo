#include "calka.h"

Calka::~Calka()
{

}

void Calka::rysuj(Dialog &dialog)
{
    QVector<double> x, y;
    double xmin = x_min, xmax = x_max, dx = 0.0001;
    while(xmin < xmax)
    {
        if(xmin + dx > xmax)
        {
            x.push_back(xmax);
            y.push_back(funkcja->wartosc(xmax));
        }

        x.push_back(xmin);
        y.push_back(funkcja->wartosc(xmin));

        xmin += dx;
    }
    dialog.rysuj(x, y, x_min, x_max, funkcja->minimum(x_min, x_max), funkcja->maximum(x_min, x_max));
}
