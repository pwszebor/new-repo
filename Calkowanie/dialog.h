#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>
#include "qcustomplot.h"

namespace Ui {
class Dialog;
}

class Dialog : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog(QWidget *parent = 0);
    ~Dialog();

    void rysuj(QVector<double> &x, QVector<double> &y, double xmin, double xmax, double ymin, double ymax);

private:
    Ui::Dialog *ui;
};

#endif // DIALOG_H
