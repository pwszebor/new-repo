#include "wielomian.h"
#include "math.h"

Wielomian::~Wielomian()
{

}

double Wielomian::wartosc(double x)
{
    double wynik = 0;
    int j = 0;

    for(auto i : wspolczynniki)
    {
        wynik += i * pow(x, j);
        j++;
    }
    return wynik;
}

double Wielomian::minimum(double x1, double x2)
{
    double dx = 0.001, min = wartosc(x1);
    while(x1 < x2)
    {
        if(wartosc(x1) < min)
            min = wartosc(x1);
        x1 += dx;
    }
    return min;
}

double Wielomian::maximum(double x1, double x2)
{
    double dx = 0.001, max = wartosc(x1);
    while(x1 < x2)
    {
        if(wartosc(x1) > max)
            max = wartosc(x1);
        x1 += dx;
    }
    return max;
}
