#ifndef WIELOMIAN_H
#define WIELOMIAN_H

#include <vector>
#include "funkcja.h"

class Wielomian : public Funkcja
{
public:
    Wielomian(const std::vector<double> &w)
        : wspolczynniki(w)
    {}
    ~Wielomian();

    double wartosc(double x);
    double minimum(double, double);
    double maximum(double, double);
private:
    std::vector<double> wspolczynniki;
};

#endif // WIELOMIAN_H
