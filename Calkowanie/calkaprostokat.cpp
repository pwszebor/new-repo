#include "calkaprostokat.h"
#include "exception.h"

CalkaProstokat::CalkaProstokat(double xmin, double xmax, Funkcja *f)
    : Calka(xmin, xmax, f)
{

}

CalkaProstokat::~CalkaProstokat()
{

}

double CalkaProstokat::obliczWartosc()
{
    try
    {
        if(getMin() > getMax())
            throw Exception(BLEDNE_DANE);
        else
        {
            double wynik = 0;
            double dx = 0.00001;
            double x1 = getMin();
            double x2 = getMax();

            while(getMin() < getMax())
            {
                if(getMin() + dx > getMax())
                    dx = getMax() - getMin();

                wynik += dx * getF()->wartosc(getMin() + dx/2);

                setMin(getMin() + dx);
            }

            setMin(x1);
            setMax(x2);
            return wynik;
        }
    }
    catch(Exception &)
    {
        throw;
    }
}
