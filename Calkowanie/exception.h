#ifndef EXCEPTION
#define EXCEPTION

#include <QString>

enum ErrType { BLEDNE_DANE };

class Exception
{
public:
    Exception(int err)
        : error(err)
    {}
    ~Exception() {}

    void info()
    {
        if(error == BLEDNE_DANE)
            qDebug("Bledne dane");
    }

private:
    int error;
};

#endif // EXCEPTION

