#include "dialog.h"
#include "ui_dialog.h"

Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);

    std::vector<double> wspol = {0, 1, 0, -0.16, 0, 0.0083};
    Wielomian wielomian(wspol);
    CalkaProstokat calka(-2, 3, &wielomian);
    CalkaTrapez calka2(-2, 3, &wielomian);
    /*try
    {
        calka.rysuj(w);
        qDebug("%f", calka.obliczWartosc());
        qDebug("%f", calka2.obliczWartosc());
    }
    catch(Exception &e)
    {
        e.info();
    }*/
}

Dialog::~Dialog()
{
    delete ui;
}

void Dialog::rysuj(QVector<double> &x, QVector<double> &y, double xmin, double xmax, double ymin, double ymax)
{
    ui->widget->addGraph();
    ui->widget->graph(0)->addData(x, y);

    ui->widget->xAxis->setLabel("x");
    ui->widget->yAxis->setLabel("y");

    ui->widget->xAxis->setRange(xmin, xmax);
    ui->widget->yAxis->setRange(ymin, ymax);
    ui->widget->replot();
}
