#include "dialog.h"
#include "calkaprostokat.h"
#include "calkatrapez.h"
#include "wielomian.h"
#include "exception.h"
#include <QApplication>

int main(int argc, char *argv[]) {
    QApplication a(argc, argv);
    Dialog w;
    w.show();

    return a.exec();
}
