#ifndef OKNODIALOGOWE_H
#define OKNODIALOGOWE_H

#include <QDialog>

namespace Ui {
class Oknodialogowe;
}

class Oknodialogowe : public QDialog
{
    Q_OBJECT

public:
    explicit Oknodialogowe(QWidget *parent = 0);
    ~Oknodialogowe();

private slots:
    void functionChange(int);

private:
    Ui::Oknodialogowe *ui;
};

#endif // OKNODIALOGOWE_H
