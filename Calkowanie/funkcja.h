#ifndef FUNKCJA_H
#define FUNKCJA_H


class Funkcja
{
public:
    Funkcja();
    virtual ~Funkcja();

    virtual double wartosc(double) = 0;
    virtual double minimum(double, double) = 0;
    virtual double maximum(double, double) = 0;
};

#endif // FUNKCJA_H
