#ifndef CALKAPROSTOKAT_H
#define CALKAPROSTOKAT_H

#include "calka.h"

class CalkaProstokat : public Calka
{
public:
    CalkaProstokat(double xmin, double xmax, Funkcja *f);
    ~CalkaProstokat();

    double obliczWartosc();
};

#endif // CALKAPROSTOKAT_H
