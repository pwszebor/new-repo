#-------------------------------------------------
#
# Project created by QtCreator 2015-06-16T12:28:32
#
#-------------------------------------------------

QT       += core gui printsupport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Calkowanie
TEMPLATE = app


SOURCES += main.cpp\
        dialog.cpp \
    calka.cpp \
    calkaprostokat.cpp \
    calkatrapez.cpp \
    funkcja.cpp \
    wielomian.cpp \
    qcustomplot.cpp \
    oknodialogowe.cpp

HEADERS  += dialog.h \
    calka.h \
    calkaprostokat.h \
    calkatrapez.h \
    funkcja.h \
    wielomian.h \
    exception.h \
    qcustomplot.h \
    oknodialogowe.h

FORMS    += dialog.ui \
    oknodialogowe.ui
